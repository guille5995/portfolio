﻿/*
 * Autor: Guillermo Rodríguez
 * 
 * Descripción: Script que gestiona lo referente a nodos de desvío
*/
using UnityEngine;
using System.Collections;

public class Switch : Node {

    //Booleano para evitar llamar a una corrutina una vez llamada
    private bool coroutineCalled = false;

    //Inicializacion
    void Start ()
    {
        //Al comenzar, ponemos todos los semaforos en verde
        foreach (TrafficLight switchLight in semaphore)
        {
            switchLight.SetRed();
        }
    }

    //Función que activa la corrutina para comenzar la gestión de tráfico con un semaforo indicado
    public void SwitchPass(TrafficLight light)
    {
        
        StartCoroutine(ChangeGreen(1, light));
    }

    //Corrutina para hacer los cambios a semaforos y gestionar el trafico.
    IEnumerator ChangeGreen(float time, TrafficLight light)
    {
        //Si la corrutina se esta ejecutando, salgo
        if (coroutineCalled)
            yield break;

        //Activo el bool para indicar que la corrutina se esta ejecutando
        coroutineCalled = true;

        //Pongo en rojo cada semforo
        foreach (TrafficLight switchLight in semaphore)
        {
            switchLight.SetRed();
        }

        //Espero un tiempo
        yield return new WaitForSeconds(time);

        //Pongo en verde el semaforo que se ha indicado
        light.SetGreen();

        //Indico que se puede volver a llamar a la corrutina
        coroutineCalled = false;

    }
}
