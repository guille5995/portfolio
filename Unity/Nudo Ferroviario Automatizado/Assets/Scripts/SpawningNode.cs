﻿/*
 * Autor: Guillermo Rodríguez
 * 
 * Descripción: Script que contiene y gestiona lo referente a nodos de spawn
*/
using UnityEngine;
using System.Collections;


public class SpawningNode : Node {

    //Referencia al prefab del tren
    public GameObject trainPrefab;
    //Referencia al tren ya instanciado en la escena
    private GameObject train;
    //Booleano para ver si puede spawnear otro tren
    public bool canSpawn = true;
    //Array de rutas serializado para poder editarlo más comodamente en el editor
    [SerializeField]Routes[] routeList;

    //Inicializacion
    void Start () 
	{
        //Instancio el tren en la escena, le doy una ruta aleatoria que contenga en nodo, y no permito que hagan mas spawn
        train = (GameObject)Instantiate(trainPrefab, transform.position, transform.rotation);
        train.GetComponent<Train>().currentRoute = routeList[Random.Range(0, routeList.Length)];
        canSpawn = false;
        
	}

	//Si pueden volver a spawnear trenes, hago lo mismo que en start
	void Update () 
	{
        if(canSpawn == true)
        {
            train = (GameObject)Instantiate(trainPrefab, transform.position, transform.rotation);
            train.GetComponent<Train>().currentRoute = routeList[Random.Range(0, routeList.Length)];
            canSpawn = false;
        }
    }
}
