﻿/*
 * Autor: Guillermo Rodríguez
 * 
 * Descripción: Script que gestiona y contiene lo referente a semáforos
*/
using UnityEngine;

public class TrafficLight : MonoBehaviour
{
    //Posee dos materiales para indicar si se puede pasar o no
    public Material red;
    public Material green;
    //Un booleano para comprobar si se puede pasar
    public bool pass;
	
    //Funcion que niega el paso y pone el semaforo en rojo
	public void SetRed()
    {
        gameObject.GetComponent<Renderer>().material = red;
        pass = false;
    }

    //Funcion que permite el paso y pone el semaforo en verde
    public void SetGreen()
    {
        gameObject.GetComponent<Renderer>().material = green;
        pass = true;
    }
}
