﻿/*
 * Autor: Guillermo Rodríguez
 * 
 * Descripción: Script que gestiona todo lo referente los nodos que representan travesías
 * 
*/
using UnityEngine;
using System.Collections;

public class Crossing : Node {

    //Booleano para evitar llamar a una corrutina una vez llamada
    private bool coroutineCalled = false;

	// Initializacion
	void Start ()
    {
        //Al comenzar, ponemos todos los semaforos en verde
        foreach (TrafficLight switchLight in semaphore)
        {
            switchLight.SetGreen();
        }
    }
	
    //Función que activa la corrutina para comenzar la gestión de tráfico con un semaforo indicado
    public void CrossingPass(TrafficLight light)
    {

        StartCoroutine(ChangeGreen(1, light));
    }

    //Corrutina para hacer los cambios a semaforos y gestionar el trafico.
    IEnumerator ChangeGreen(float time, TrafficLight light)
    {
        //Si la corrutina se esta ejecutando, salgo
        if (coroutineCalled)
            yield break;

        //Activo el bool para indicar que la corrutina se esta ejecutando
        coroutineCalled = true;

        //Pongo en rojo cada semforo
        foreach (TrafficLight switchLight in semaphore)
        {
            switchLight.SetRed();
        }

        //Pongo en verde solo por el que el tren va a pasar 
        light.SetGreen();

        yield return new WaitForSeconds(time);

        //Vuelvo a poner todos los semaforos en verde
        foreach (TrafficLight switchLight in semaphore)
        {
            switchLight.SetGreen();
        }

        //Indico que se puede volver a llamar a la corrutina
        coroutineCalled = false;

    }
}
