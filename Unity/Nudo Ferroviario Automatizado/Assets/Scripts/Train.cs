﻿/*
 * Autor: Guillermo Rodríguez
 * 
 * Descripción: Script que gestiona todo lo referente al recorrido del tren por las rutas
 * 
*/

using UnityEngine;
using System.Collections;

public class Train : MonoBehaviour
{
    // Velocidad de el tren
    public float speed;

    //Ruta que este tren va a seguir
    public Routes currentRoute;

    //Contador usado para ver en que punto de la ruta se halla el tren. Lo inicializo en start.
    private int i;

    //Booleanos usados para ver si el tren se ha de mover, si ha de comenzar la corrutina, y para evitar llamarla si ya  se esta se esta ejecutando
    private bool moveOn = true;
    private bool coroutineCalled = false;
    private bool startCoroutine = false;

    //Se;al a la cual debe atender el tren en este momento
    private TrafficLight currentSemaphore;


    //Inicializacion
    void Start()
    {
        
        i = 1;

    }

    // En update gestiono todo el movimiento del tren
    void Update()
    {
        //Esta variable representa lo que avanza poor frame el tren. A mayor velocida, mayor es el avance
        float step = speed * Time.deltaTime;

        //En caso de que el tren se halle en el nodo al que se dirigia
        if (transform.position == Vector3.MoveTowards(transform.position, currentRoute.nodeList[i].transform.position, step))
        {
            //Si este nodo es de tipo Despawn
            if (currentRoute.nodeList[i].tag == "Despawn")
            {
                //Lo eliminamos y deajamos que otro haga spawn
                Destroy(gameObject);
                currentRoute.nodeList[0].GetComponent<SpawningNode>().canSpawn = true;
            }

            //Si hay mas nodos, aumentamos el contador para avanzar en la ruta
            if (i < currentRoute.nodeList.Length - 1)
            {
           
                i++;

            }

            //Si estamos en el nodo previo a un Switch o a un Crossing
            if (currentRoute.nodeList[i].tag == "Switch" || currentRoute.nodeList[i].tag == "Crossing")
            { 
                //Comparamos los semaforos de cada nodo con los semaforos previstos en la ruta. El que coincida se lo pasamos currentSemaphore
                foreach (TrafficLight routeLight in currentRoute.semaphoreList)
                {
                    foreach (TrafficLight nodeLight in currentRoute.nodeList[i].semaphore)
                    {
                        if (routeLight == nodeLight)
                        {
                            currentSemaphore = nodeLight;
                        }
                    }
                }

                //Permitimos el comienzo de la corrutina
                startCoroutine = true;

            }

        }

        //Comenzamos la corrutina si se permite
        if(startCoroutine)
        {
            StartCoroutine(WaitTraffic(1, currentSemaphore));
        }

        //Movemos el tren si se permite
        if(moveOn) 
        {
            transform.position = Vector3.MoveTowards(transform.position, currentRoute.nodeList[i].transform.position, step);
        }

        //LookAt hace que el tren mire hacia el nodo al que va
        transform.LookAt(currentRoute.nodeList[i].transform);

    }

    
    //Esta corrutina se ejecuta para hacer que el tren espere mientras comprueba si puede pasar
    IEnumerator WaitTraffic(float time, TrafficLight light)
    {
        //Si la corrutina ya ha sido llamada o el semaforo esta en verde
        if ((coroutineCalled || light.pass))
        {
            //Si el nodo actual es de tipo Crossing
            if (currentRoute.nodeList[i].tag == "Crossing")
            {
                //Ejecutamos su comportamiento
                currentRoute.nodeList[i].GetComponent<Crossing>().CrossingPass(light);

            }
            //Salimos de la corrutina
            yield break;
        }
        //Para evitar que el tren se mueva y que vuelva a llamar a la corrutina, cambiamos los booleanos
        moveOn = false;
        coroutineCalled = true;

        //Si el nodo actual es de tipo Switch, ejecutamos su comportamiento
        if (currentRoute.nodeList[i].tag == "Switch")
            currentRoute.nodeList[i].GetComponent<Switch>().SwitchPass(light);

        //Si el nodo actual es de tipo Crossing, ejecutamos su comportamiento
        if (currentRoute.nodeList[i].tag == "Crossing")
        {
            currentRoute.nodeList[i].GetComponent<Crossing>().CrossingPass(light);
            
        }

        //Esperamos un tiempo
        yield return new WaitForSeconds(time);

        //Tras esperar, si el semaforo esta en verde, reanudamos la marcha y se deja de llamar a la corrutina
        if (light.pass)
        {
            moveOn = true;
            startCoroutine = false;
        }

        //Cambiamos el booleano para poder volver a llamar a la corrutina
        coroutineCalled = false;

    }
}
