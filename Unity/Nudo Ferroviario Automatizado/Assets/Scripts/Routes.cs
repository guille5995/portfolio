﻿/*
 * Autor: Guillermo Rodríguez
 * 
 * Descripción: Script que contiene la información de una ruta
*/
using System;

[Serializable]
public class Routes
{
    //Posee un listado de nodos y un listado de semaforos en la ruta. El listado de nodos ha de estar ordenado en orden de paso para reorrerlos correctamente
    public Node[] nodeList;
    public TrafficLight[] semaphoreList;

}
