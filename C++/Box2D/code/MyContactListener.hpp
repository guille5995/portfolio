/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*																				*
*  Author: Guillermo Rodríguez Rubio											*
*																				*
*																				*
*  Date: March 2017															    *
*																				*
*  guille5995@gmail.com														    *
*																				*
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef CONTACT_HEADER
#define CONTACT_HEADER

#include <Box2D\Box2D.h>
#include "Entity.hpp"

#pragma once

class MyContactListener : public b2ContactListener
{
	void BeginContact(b2Contact* contact) {

		//Obtener datos de la fixture A
		void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
		if (bodyUserData)
			static_cast<Entity*>(bodyUserData)->startContact();
		
		//Comprobar si fixture B es una entidad

		if (contact->GetFixtureB()->IsSensor())
		{
			void* bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
			if (bodyUserData)
				static_cast<Entity*>(bodyUserData)->startContact();

		}
		

	}

	void EndContact(b2Contact* contact) {

		//Obtener datos de la fixture A
		void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
		if (bodyUserData)
			static_cast<Entity*>(bodyUserData)->endContact();

		//Comprobar si fixture B es una entidad

		if (contact->GetFixtureB()->IsSensor())
		{
			bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
			if (bodyUserData)
				static_cast<Entity*>(bodyUserData)->endContact();
		}

	}
};

#endif // !CONTACT_HEADER