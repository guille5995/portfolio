/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*																				*
*  Author: Guillermo Rodríguez Rubio											*
*																				*
*																				*
*  Date: March 2017															    *
*																				*
*  guille5995@gmail.com														    *
*																				*
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "Scene.hpp"

void Scene::create_physics_world(float world_width, float world_height)
{
	physics_world.SetContactListener(&contactListenerInstance);

	// VEHICULO
	{
		Box * carBody = new Box(50.f, 175.f, 0, 25.f, 10.f);
		add_entity("carBody", carBody);
		carBody->setColor(Color::Cyan);
		
		
		//RUEDAS

		Circle * wheel01 = new Circle(10.f, 50.f, 175.f);
		carBody->add_child(wheel01);
		add_entity("wheel01", wheel01);

		Circle * wheel02 = new Circle(10.f, 50.f, 175.f);
		carBody->add_child(wheel02);
		add_entity("wheel02", wheel02);

		carBody->add_wheel(wheel01->getBody(), 20.f, -10.f, physics_world);

		carBody->add_wheel(wheel02->getBody(), -20.f, -10.f, physics_world);

		//CESTA

		Box * basketLeft = new Box(50.f, 200.f, 0, 15.f, 5.f);
		carBody->add_child(basketLeft);
		add_entity("basketLeft", basketLeft);
		basketLeft->setColor(Color::Magenta);

		carBody->add_revolute_joint(basketLeft->getBody(), -25.f, 20.f, -15.f, 0.f, -((2.f*3.14f) / 4.f), physics_world);

		Box * basketCenter = new Box(50.f, 200.f, 0, 25.f, 5.f);
		carBody->add_child(basketCenter);
		add_entity("basketCenter", basketCenter);
		basketCenter->setColor(Color::Magenta);

		basketLeft->add_revolute_joint(basketCenter->getBody(), -10.f, 0.f, -25.f, 0.f, ((2.f*3.14f) / 4.f), physics_world);

		Box * basketRight = new Box(50.f, 200.f, 0, 15.f, 5.f);
		carBody->add_child(basketRight);
		add_entity("basketRight", basketRight);
		basketRight->setColor(Color::Magenta);

		basketCenter->add_revolute_joint(basketRight->getBody(), 25.f, 0.f, -10.f, 0.f, -((2.f*3.14f) / 4.f), physics_world);

		carBody->getBody()->SetTransform(carBody->getBody()->GetPosition(), 0.f);


	}

	//SUELO
	{

		Box * floor01 = new Box(75.f, 75.f, 0, 75.f, 75.f);
		add_entity("floor01", floor01);

		floor01->getBody()->SetType(b2_staticBody);
		floor01->setColor(Color::Yellow);

		Polygon * floor02 = new Polygon
			(
				175.f,
				0.f,
				{
					b2Vec2(-25,  0),
					b2Vec2(-25, 150),
					b2Vec2(25, 100),
					b2Vec2(25,  0),
				}
		);
		add_entity("floor02", floor02);
		floor02->setColor(Color::Yellow);


		Polygon * floor03 = new Polygon
			(
				225.f,
				0.f, 
				{
					b2Vec2(-25,  0),
					b2Vec2(-25, 100),
					b2Vec2(25, 75),
					b2Vec2(25,  0),

				}
		);
		add_entity("floor03", floor03);
		floor03->setColor(Color::Yellow);

		Polygon * floor04 = new Polygon
			(
				275.f, 
				0.f, 
				{
					b2Vec2(-25,  0),
					b2Vec2(-25, 75),
					b2Vec2(25, 75),
					b2Vec2(25,  0),

				}
		);
		add_entity("floor04", floor04);
		floor04->setColor(Color::Yellow);

		Polygon * floor05 = new Polygon
			(
				325.f,
				0.f,
				{
					b2Vec2(-25,  0),
					b2Vec2(-25, 75),
					b2Vec2(50, 100),
					b2Vec2(50,  0),

				}
		);
		add_entity("floor05", floor05);
		floor05->setColor(Color::Yellow);


		Polygon * floor06 = new Polygon
			(
				600.f,
				0.f, 
				{
					b2Vec2(-100,  0),
					b2Vec2(-100, 50),
					b2Vec2(50, 75),
					b2Vec2(50,  0),

				}
		);
		add_entity(" floor06", floor06);
		floor06->setColor(Color::Yellow);


		Polygon * floor07 = new Polygon
			(
				700.f, 
				0.f, 
				{
					b2Vec2(-50,  0),
					b2Vec2(-50, 75),
					b2Vec2(100, 100),
					b2Vec2(100,  0),

				}
		);
		add_entity("floor07", floor07);
		floor07->setColor(Color::Yellow);
	}

	//CESTAS
	{
		Circle * jointPoint01 = new Circle(10.f, 600.f, 375.f);
		add_entity("jointPoint01", jointPoint01);
		jointPoint01->getBody()->SetType(b2_kinematicBody);

		Box * basketLeft01 = new Box(600.f, 400.f, 0, 10.f, 50.f);
		add_entity("basketLeft01", basketLeft01);
		basketLeft01->setColor(Color::Yellow);

		Box * basketRight01 = new Box(60.f, 400.f, 0, 10.f, 50.f);
		add_entity("basketRight01", basketRight01);
		basketRight01->setColor(Color::Yellow);

		jointPoint01->add_revolute_joint(basketLeft01->getBody(), 0, 0.f, 0.f, -50.f, ((2.f*3.14f) / 8.f), physics_world);

		jointPoint01->add_revolute_joint(basketRight01->getBody(), 0, 0.f, 0.f, -50.f, ((2.f*3.14f) / 8.f), physics_world);

		basketLeft01->add_revolute_joint(basketRight01->getBody(), -50.f, 0.f, 50.f, 0.f, 0, physics_world);

		Box * pilar01 = new Box(600.f, 100.f, 0.f, 10.f, 275.f);
		add_entity("pilar01", pilar01);

		pilar01->getBody()->SetType(b2_staticBody);
		pilar01->getBody()->SetActive(false);
		pilar01->setColor(Color::Yellow);


	}
	//ASCENSOR
	{

		Box * elevator = new Box(438.f, -50.f, 0, 61.f, 15.f);
		add_entity("elevator", elevator);

		elevator->getBody()->SetType(b2_kinematicBody);
		elevator->getBody()->SetActive(false);
		
		elevator->setColor(Color::Red);
	}

	//PLATAFORMA
	{
		Box * platform01 = new Box(325.f, 400.f, 0, 50.f, 10.f);
		add_entity("platform01", platform01);

		platform01->setColor(Color::Yellow);

		platform01->getBody()->SetType(b2_staticBody);

		Box * platform02 = new Box(240.f, 380.f, 0.5f, 50.f, 10.f);
		add_entity("platform02", platform02);

		platform02->getBody()->SetType(b2_staticBody);
		platform02->setColor(Color::Yellow);

		Box * platform03 = new Box(165.f, 380.f, -0.5f, 50.f, 10.f);
		add_entity("platform03", platform03);

		platform03->getBody()->SetType(b2_staticBody);
		platform03->setColor(Color::Yellow);

	}

	//PELOTAS
	{

		float height = 650;

		for (auto i = 0; i< 7; i++)
		{
			Circle * circle = new Circle(5.f, 600.f, height);
			add_entity("ball" + std::to_string(i), circle);
			height += 10.f;

			circle->setColor(Color::Green);

		}

	}

	//SENSORES
	{
		Box * sensor01 = new Box(750.f, 150.f, 0.175f, 50.f, 50.f);
		add_entity("sensor01", sensor01);

		sensor01->getBody()->SetType(b2_staticBody);
		sensor01->getBody()->GetFixtureList()->SetSensor(true);
		sensor01->setRendering(false);

		Box * sensor02 = new Box(438.f, 75.f, 0, 40.f, 15.f);
		add_entity("sensor02", sensor02);

		sensor02->getBody()->SetType(b2_staticBody);
		sensor02->getBody()->GetFixtureList()->SetSensor(true);
		sensor02->setRendering(false);

		Polygon * sensor03 = new Polygon
			(
				202.f,
				370.f,
				{
					b2Vec2(0,  0),
					b2Vec2(-75, 40),
					b2Vec2(75, 40),
					

				}
		);
		add_entity("sensor03", sensor03);

		sensor03->getBody()->GetFixtureList()->SetSensor(true);
		sensor03->setRendering(false);

	}

	
}
