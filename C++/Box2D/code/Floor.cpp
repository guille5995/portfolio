#include "Floor.hpp"

Floor::Floor(b2Vec2 startPoint, b2Vec2 endPoint, shared_ptr< b2World > physics_world)
{
	// Se crea el body a partir de una definición de sus características:

	b2BodyDef body_definition;

	body_definition.type = b2_staticBody;
	body_definition.position.Set(0.f, 50.f);                                       // Posición inicial absoluta
	body_definition.angle = 0.f;

	b2Body * body = physics_world->CreateBody(&body_definition);

	// Se añande una fixture al body:

	b2EdgeShape body_shape;

	body_shape.Set(startPoint, endPoint);                    // Coordenadas locales respecto al centro del body

	b2FixtureDef body_fixture;

	body_fixture.shape = &body_shape;

	body->CreateFixture(&body_fixture);

}