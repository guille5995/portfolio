/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*																				*
*  Author: Guillermo Rodr�guez Rubio											*
*																				*
*																				*
*  Date: March 2017															    *
*																				*
*  guille5995@gmail.com														    *
*																				*
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef BOX_HEADER
#define BOX_HEADER

#include "Entity.hpp"

#pragma once

using namespace std;

class Box : public Entity
{

protected:

	float posX, posY, angle,width, height;

public:

	Box()
	{
		posX = 0;
		posY = 0;
		width = 0;
		height = 0;
	}

	Box(float _posX, float _posY, float _angle,float _width, float _height)
	{
		posX = _posX;
		posY = _posY;
		angle = _angle;

		width = _width;
		height = _height;
	}

	bool initialize(b2World & physics_world)
	{
		// Se crea el body a partir de una definici�n de sus caracter�sticas:

		b2BodyDef body_definition;

		body_definition.type = b2_dynamicBody;
		body_definition.position.Set(posX, posY);   // Posici�n inicial absoluta
		body_definition.angle = angle;
		

		body = physics_world.CreateBody(&body_definition);
		body->SetUserData(this);

		// Se a�ande una fixture al body:

		b2PolygonShape body_shape;

		body_shape.SetAsBox(width, height);

		shape = &body_shape;

		b2FixtureDef body_fixture;

		body_fixture.shape = &body_shape;
		body_fixture.density =1.00f;
		body_fixture.restitution = 0.50f;
		body_fixture.friction = 0.50f;

		body->CreateFixture(&body_fixture);

		return true;

	}

	void render(RenderTarget & target)
	{
		if (rendering)
		{


			// Se toma la forma poligonal de Box2D (siempre es convexa) y se crea a partir de sus v�rtices un
			// ConvexShape de SFML. Cada v�rtice de Box2D hay que transformarlo usando el transform del body.

			b2PolygonShape * box2d_polygon = dynamic_cast<b2PolygonShape *>(body->GetFixtureList()->GetShape());
			ConvexShape       sfml_polygon;

			int number_of_vertices = box2d_polygon->GetVertexCount();

			sfml_polygon.setPointCount(number_of_vertices);
			sfml_polygon.setFillColor(color);

			for (int index = 0; index < number_of_vertices; index++)
			{
				sfml_polygon.setPoint
					(
						index,
						box2d_position_to_sfml_position
						(
							b2Mul(body->GetTransform(), box2d_polygon->GetVertex(index)), 
							target.getSize().y)
						);
			}

			target.draw(sfml_polygon);

		}
	}
};

#endif // !BOX_HEADER