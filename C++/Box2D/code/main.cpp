/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Started by Ángel on april of 2016										   *
 *		                                                                       *
 *	Modified by Guillermo Rodríguez Rubio                                      *
 *                                                                             *
 *  This is free software released into the public domain.                     *
 *                                                                             *
 *  angel.rodriguez@esne.edu                                                   *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <memory>
#include <vector>
#include <Box2D/Box2D.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Scene.hpp"

using namespace sf;
using namespace std;
//using namespace Engine;

int main ()
{
    RenderWindow window(VideoMode(800, 600), "Practica de Animacion 01: Guillermo Rodriguez Rubio", Style::Titlebar | Style::Close, ContextSettings(32));

    window.setVerticalSyncEnabled (true);
	Scene scene(800, 600);
    //shared_ptr< b2World > physics_world = scene.create_physics_world (800, 600);

	float delta_time = 0.017f;

    bool running = true;

    Clock timer;
    

    do
    {
        timer.restart ();

        // Process window events:

        Event event;

        while (window.pollEvent (event))
        {
			if (event.type == Event::KeyPressed)
			{

				if (event.key.code == Keyboard::Right)
				{
					scene.activateMotorsForward();
				}

				if (event.key.code == Keyboard::Left)
				{
					scene.activateMotorsBackwards();
				}

				if (event.key.code == Keyboard::Down)
				{
					scene.wheelsBrake();
				}
				

				if (event.key.code == Keyboard::Up)
				{
					scene.activateBasket();

				}

				if (event.key.code == Keyboard::Escape)
				{
					running = false;

				}
			}

            if (event.type == Event::Closed)
            {
                running = false;
            }

        }

        // Update:
		

		scene.get_physics_world().Step(delta_time, 8, 4);

		scene.checkScene();
		

        // Render:

        window.clear ();

        scene.render (window);

        window.display ();

        delta_time = (delta_time + timer.getElapsedTime ().asSeconds ()) * 0.5f;
    }
    while (running);

    return EXIT_SUCCESS;
}
