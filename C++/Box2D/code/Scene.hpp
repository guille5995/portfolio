/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*																				*
*  Author: Guillermo Rodr�guez Rubio											*
*																				*
*																				*
*  Date: March 2017															    *
*																				*
*  guille5995@gmail.com														    *
*																				*
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef SCENE_HEADER
#define SCENE_HEADER

#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>

#include "Box.hpp"
#include "Floor.hpp"
#include "Entity.hpp"
#include "Circle.hpp"
#include "Polygon.hpp"
#include "MyContactListener.hpp"

#pragma once
using namespace sf;
using namespace std;


class Scene
{
		

private:

		b2World physics_world;

		map< string, shared_ptr< Entity > > entities;

		float delta_time = 0.017f;          // ~60 fps

		MyContactListener contactListenerInstance;

		float liftHeightLimit = 35.f;

public:

		Scene() : physics_world(b2Vec2(0, -100.f))
		{
			create_physics_world(0, 0);			
		}

		Scene(float physics_width, float physics_height) : physics_world(b2Vec2(0, -100.f))
		{
			create_physics_world(physics_width, physics_height);
		}

		b2World & get_physics_world() { return physics_world; }

		//M�todo de inicializaci�n de entidades que tambien las a�ade al map
		bool add_entity(const string & name, Entity * entity)
		{
			if (entities.count(name) == 0)
			{
				if (entity->initialize(physics_world))
				{
					entities[name].reset (entity);
					return true;
				}
			}
			return false;
		}

		
		//M�todo de renderizado
		void render(RenderTarget & target)
		{
			for (auto const & entity : entities)
			{
				entity.second->render(target);
			}
		}

		void create_physics_world(float world_width, float world_height);

		//Comportamientos del vehiculo
		void activateMotorsForward()
		{

			entities.at("carBody")->activateMotorsForward();
			
		}

		void activateMotorsBackwards()
		{

			entities.at("carBody")->activateMotorsBackwards();

		}

		void wheelsBrake()
		{

			entities.at("carBody")->wheelsBrake();

		}


		//Comportamientos de la escena
		void activateBasket()
		{
			for each(b2RevoluteJoint * joint in entities.at("carBody")->getRevoluteJointList())
			{
				
				joint->SetLimits(-((2.f*3.14f) / 4.f), 0);
				joint->SetMotorSpeed(-(joint->GetMotorSpeed()));
				
				joint->EnableMotor(true);
				
					
			}
		}

		void rotateBallBasket()
		{
			entities.at("jointPoint01")->getBody()->SetAngularVelocity(-1.f);
		}

		void raiseLift()
		{
			entities.at("elevator")->getBody()->SetActive(true);
			entities.at("elevator")->getBody()->SetLinearVelocity(b2Vec2(0, 50));
		}
		
		//M�todo a ejecutar en el Update que comprueba factores indicados en la escena
		void checkScene()
		{

			if (entities.at("carBody")->getBody()->GetPosition().y < 0)
			{
				entities.at("carBody")->getBody()->SetTransform(b2Vec2(50.f, 175.f), 0);

				for each(Entity * child in entities.at("carBody")->getChilden())
				{
					child->getBody()->SetTransform(b2Vec2(50.f, 175.f), 0);
				}
			}
			
			if (entities.at("sensor01")->getBody()->GetContactList())
			{
				if (entities.at("sensor01")->getBody()->GetContactList()->contact->GetFixtureA()->GetBody() == entities.at("carBody")->getBody())
				{
					raiseLift();
					rotateBallBasket();

				}
			}

			if (entities.at("sensor02")->getBody()->GetContactList())
			{
				if (entities.at("sensor02")->getBody()->GetContactList()->contact->GetFixtureA()->GetBody() == entities.at("carBody")->getBody() && entities.at("elevator")->getBody()->GetPosition().y >= 0)
				{
					raiseLift();
					liftHeightLimit = 400.f;
				}
				
			}

			if (entities.at("elevator")->getBody()->GetPosition().y >= liftHeightLimit)
				entities.at("elevator")->getBody()->SetLinearVelocity(b2Vec2(0, 0));

			if (entities.at("jointPoint01")->getBody()->GetAngle() <= -1.5)
				entities.at("jointPoint01")->getBody()->SetAngularVelocity(0);

		}

	};

#endif // !SCENE_HEADER
