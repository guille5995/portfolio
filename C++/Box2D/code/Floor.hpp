#include <Box2D/Box2D.h>
#include <vector>

#pragma once

using namespace std;

class Floor
{

public:

	Floor(b2Vec2 startPoint, b2Vec2 endPoint, shared_ptr< b2World > physics_world);
};