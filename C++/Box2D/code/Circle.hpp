/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*																				*
*  Author: Guillermo Rodr�guez Rubio											*
*																				*
*																				*
*  Date: March 2017															    *
*																				*
*  guille5995@gmail.com														    *
*																				*
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef CIRCLE_HEADER
#define CIRCLE_HEADER

#include "Entity.hpp"

#pragma once

using namespace sf;
using namespace std;


class Circle : public Entity
{

protected:

	float radius, posX, posY;


public:

	Circle()
	{
		radius = 0;
		posX = 0;
		posY = 0;

	}

	Circle(float physics_radius, float _posX, float _posY)
	{
		radius = physics_radius;
		posX = _posX;
		posY = _posY;
		
	}

	bool initialize(b2World & physics_world)
	{
			b2BodyDef body_definition;

            body_definition.type = b2_dynamicBody;
            body_definition.position.Set (posX, posY);// Posici�n inicial absoluta

            body = physics_world.CreateBody (&body_definition);
			body->SetUserData(this);


            // Se a�ande una fixture al body:

            b2CircleShape body_shape;

            body_shape.m_radius = radius;

			shape = &body_shape;

            b2FixtureDef body_fixture;

            body_fixture.shape       = &body_shape;
            body_fixture.density     = 3.00f;
            body_fixture.restitution = 0.75f;
            body_fixture.friction    = 0.50;

            body->CreateFixture (&body_fixture);

		return true;

	}

	void render(RenderTarget & target) 
	{
		if (rendering) 
		{
			// Se crea un CircleShape a partir de los atributos de la forma de la fixture y del body:
			// En SFML el centro de un c�rculo no est� en su position. Su position es la esquina superior izquierda
			// del cuadrado en el que est� inscrito. Por eso a position se le resta el radio tanto en X como en Y.

			b2CircleShape * circle = dynamic_cast< b2CircleShape * >(body->GetFixtureList()->GetShape());

			float  radius = circle->m_radius;
			b2Vec2 center = circle->m_p;

			CircleShape shape;

			shape.setPosition(box2d_position_to_sfml_position(b2Mul(body->GetTransform(), center), target.getSize().y) - Vector2f(radius, radius));
			shape.setFillColor(color);
			shape.setRadius(radius);

			target.draw(shape);
		}
		
	}
};

#endif // !CIRCLE_HEADER

