#include <Box2D/Box2D.h>
#pragma once

class Ray
{

private:

	
	b2RayCastInput input;
	b2RayCastOutput output;


	Ray() 
	{
		input.p1 = b2Vec2(0,0);
		input.p2 = b2Vec2(0, 0);
		input.maxFraction = 1.f;

		output.fraction = 0.f;
		output.normal = b2Vec2(0, 0);

	}

	Ray(b2Vec2 _p1, b2Vec2 _p2, float32 _maxFraction, float32 _fraction, b2Vec2 _normal)
	{
		input.p1 = _p1;
		input.p2 = _p2;
		input.maxFraction = _maxFraction;

		output.fraction = _fraction;
		output.normal = _normal;

	}


	


};