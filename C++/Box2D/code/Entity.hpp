/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*																				*
*  Author: Guillermo Rodr�guez Rubio											*
*																				*
*																				*
*  Date: March 2017															    *
*																				*
*  guille5995@gmail.com														    *
*																				*
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ENTITY_HEADER
#define ENTITY_HEADER


#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <list>

//#include "Scene.hpp"
#pragma once

using namespace sf;
using namespace std;


class Scene;

class Entity
{
protected:

	b2Body * body;

	b2Shape * shape;

	list<b2RevoluteJoint *> revoluteJointList;
	list<b2RevoluteJoint *> wheelList;
	list<Entity *>children;

	bool m_contacting = false;

	bool rendering = true;

	Color color = Color::White;

public:

	virtual ~Entity() {}

	virtual bool initialize(b2World & physics_world) = 0;

	virtual void update(float time)
	{
		for (auto & child : children)
		{
			child->update(time);
		}
	}
	virtual void render(RenderTarget & target)
	{
		for (auto & child : children)
		{
				child->render(target);
		}
	}

	void add_child(Entity * child)
	{
		children.push_back(child);
	}

	inline Vector2f box2d_position_to_sfml_position(const b2Vec2 & box2d_position, float window_height)
	{
		return Vector2f(box2d_position.x, window_height - box2d_position.y);
	}

	void add_wheel(b2Body * jointedBody, float posX, float posY, b2World & physics_world)
	{
		b2RevoluteJointDef revoluteJointDef;
		revoluteJointDef.bodyA = jointedBody;
		revoluteJointDef.bodyB = body;
		revoluteJointDef.collideConnected = false;

		revoluteJointDef.localAnchorA = jointedBody->GetLocalCenter();
		revoluteJointDef.localAnchorB.Set(posX, posY);

		revoluteJointDef.enableMotor = false;
		revoluteJointDef.maxMotorTorque = 1500000.f;
		revoluteJointDef.motorSpeed = 100.f; //90 degrees per second

		wheelList.push_back((b2RevoluteJoint*)physics_world.CreateJoint(&revoluteJointDef));

	}

	void add_revolute_joint(b2Body * jointedBody, float posX, float posY, float jointedPosX, float jointedposY, float angle, b2World & physics_world)
	{
		b2RevoluteJointDef revoluteJointDef;
		revoluteJointDef.bodyA = jointedBody;
		revoluteJointDef.bodyB = body;
		revoluteJointDef.referenceAngle = angle;
		revoluteJointDef.collideConnected = false;

		revoluteJointDef.localAnchorA.Set(jointedPosX, jointedposY);
		revoluteJointDef.localAnchorB.Set(posX, posY);

		revoluteJointDef.enableLimit = true;
		revoluteJointDef.upperAngle = 0.f;
		revoluteJointDef.lowerAngle = 0.f;

		revoluteJointDef.enableMotor = false;
		revoluteJointDef.maxMotorTorque = 10000000.f;
		revoluteJointDef.motorSpeed = 100.f; //90 degrees per second*/

		revoluteJointList.push_back((b2RevoluteJoint*)physics_world.CreateJoint(&revoluteJointDef));

		

	}

	//Funciones del motor del veh�culo
	void activateMotorsForward()
	{
		for each (b2RevoluteJoint * joint in wheelList)
		{
			
			joint->SetMotorSpeed(100.f);
			joint->EnableMotor(true);
			

		}
	}

	void activateMotorsBackwards()
	{
		for each (b2RevoluteJoint * joint in wheelList)
		{
			
			joint->SetMotorSpeed(-100.f);
			joint->EnableMotor(true);
			

		}
	}

	void wheelsBrake() 
	{
		for each (b2RevoluteJoint * joint in wheelList)
		{

			joint->SetMotorSpeed(0.f);

		}
	}


	//Gets y sets de varios datos
	b2Body * getBody() { return body; }
	
	list<b2RevoluteJoint *> & getRevoluteJointList() { return revoluteJointList; }
	list<Entity *> & getChilden() { return children; }

	void setColor(Color _color) { color = _color; }
	void setRendering(bool _rendering) { rendering = _rendering; }
	void startContact() { m_contacting = true; }
	void endContact() { m_contacting = false; }

	bool returnContact() { return m_contacting; }

};

#endif // !ENTITY_HEADER

